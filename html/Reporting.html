<!doctype html public "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
  <title>GNU bug tracker - how to report a bug</title>
</head>
<body>

<h1>How to report a bug</h1>

<h2>Important things to note <strong>before</strong> sending</h2>

<p>Please don't report multiple unrelated bugs - especially ones in
different packages - in a single bug report. It makes our lives much
easier if you send separate reports.

<p>You should check if your bug report has already been filed by someone
else before submitting it. Lists of currently outstanding bugs are
available <a href="./">on the World Wide Web</a> and
<a href="Access.html">elsewhere</a> - see other documents for details.
You can submit your comments to an existing bug report
#<var>&lt;number&gt;</var> by sending e-mail to
<var>&lt;number&gt;</var>@debbugs.gnu.org</p>

<!--
<p>If you can't seem to determine which package contains the problem,
please send e-mail to help-debbugs at gnu.org asking for advice.
-->

</p>

<p>If you'd like to send a copy of your bug report to additional
recipients, you shouldn't use the usual e-mail
headers, but <a href="#xcc">a different method, described below</a>.</p>

<!--
<h2>Sending the bug report using an automatic bug report tool</h2>

<p>There is a program that was developed in Debian to help reporting
bug reports, it's called
<code><a href="//packages.debian.org/stable/utils/reportbug">reportbug</a></code>.
It will guide you through the bug reporting process step by step,
and probably ease filing bugs that way.</p>

<p>Emacs users can also use the debian-bug command provided by the
<code><a href="//packages.debian.org/stable/utils/debbugs-el">
debbugs-el</a></code> package. When called with <kbd>M-x
debian-bug</kbd>, it will ask for all necessary information in a
similar way to <code>reportbug</code>.</p>
-->

<h2>Sending the bug report via e-mail</h2>

<p>Send mail to
<code>submit</code> at <code>debbugs.gnu.org</code></a>,
as described below.</p>

<p>Of course, like with any email, you should include a clear, descriptive
<code>Subject</code> line in your main mail header.  The subject you
give will be used as the initial bug title in the tracking system, so
please try to make it informative!</p>

<p>You need to put a <a name="pseudoheader">pseudo-header</a> at the start
of the body of the message. That means that the first line of the message
body should say:</p>

<pre>
Package: &lt;something&gt;
</pre>

<p>Replace <code>&lt;something&gt;</code> with the name of the package which
has the bug.</p>

<p>The second line of the message should say:</p>

<pre>
Version: &lt;something&gt;
</pre>

<p>Replace <code>&lt;something&gt;</code> with the version of the package.
Please don't include any text here other than the version itself, as the
bug tracking system relies on this field to work out which releases are
affected by the bug.</p>

<p>You need to supply a correct <code>Package</code> line in the
pseudo-header in order for the bug tracking system to deliver the message
to the package's maintainer.</p>



<p>The pseudo-header fields should start at the very start of their lines.</p>



<p>Please include in your report:</p>

<ul>
  <li>The <em>exact</em> and <em>complete</em> text of any error
      messages printed or logged.  This is very important!
  <li>Exactly what you typed or did to demonstrate the problem.
  <li>A description of the incorrect behaviour: exactly what behaviour
      you were expecting, and what you observed.  A transcript of an
      example session is a good way of showing this.
  <li>A suggested fix, or even a patch, if you have one.
  <li>Details of the configuration of the program with the problem.
      Include the complete text of its configuration files.



</ul>

<p>Include any detail that seems relevant - you are in very little danger
of making your report too long by including too much information.  If
they are small please include in your report any files you were using
to reproduce the problem (uuencoding them if they may contain odd
characters etc.).</p>


<h2><A name="example">Example</a></h2>

<p>A bug report, with mail header, looks something like this:

<pre>
  To: submit@debbugs.gnu.org
  From: diligent@testing.linux.org
  Subject: Hello says `goodbye'

  Package: hello
  Version: 1.3-16

  When I invoke `hello' without arguments from an ordinary shell
  prompt it prints `goodbye', rather than the expected `hello, world'.
  Here is a transcript:

  /usr/bin/hello
  goodbye

  I suggest that the output string, in hello.c, be corrected.

  I am using Debian GNU/Linux 2.2, kernel 2.2.17-pre-patch-13
  and libc6 2.1.3-10.
</pre>


<h2><A name="xcc">Sending copies of bug reports to other addresses</a></h2>

<p>Sometimes it is necessary to send a copy of a bug report to somewhere
else besides the package maintainer, which is where they
are normally sent.

<p>You could do this by CC'ing your bug report to the other address(es),
but then the other copies would not have the bug report number put in
the <code>Reply-To</code> field and the <code>Subject</code> line.
When the recipients reply they may end up creating a new report by mistake.

<p>The <em>right</em> way to do this is to use the <code>X-Debbugs-CC</code>
header.  Add a line like this to your message's mail header (or
to the pseudo header with the <code>Package</code> field):
<pre>
  X-Debbugs-CC: someone@example.com
</pre>
This will cause the bug tracking system to send a copy of your report
to the address(es) in the <code>X-Debbugs-CC</code> line as well as to
the package maintainer.

<p>Avoid sending such copies to the addresses of other bug reports, as
they will be caught by the checks that prevent mail loops. There is
relatively little point in using <code>X-Debbugs-CC</code> for this
anyway, as the bug number added by that mechanism will just be
replaced by a new one; use an ordinary <code>CC</code> header instead.

<p>This feature can often be combined usefully with mailing
<code>quiet</code> - see below.


<h2><A name="severities">Severity levels</a></h2>

<p>If a report is of a particularly serious bug, or is merely a feature
request that, you can set the severity level of the bug as you report
it.  This is not required, however, and the developers will assign an
appropriate severity level to your report if you do not.

<p>To assign a severity level, put a line like this one in the
<a href="#pseudoheader">pseudo-header</a>:</p>

<pre>
Severity: &lt;<var>severity</var>&gt;
</pre>

<p>Replace &lt;<var>severity</var>&gt; with one of the available severity
levels, as described in the
<a href="Developer.html#severities">developers' documentation</a>.</p>


<h2><a name="tags">Assigning tags</a></h2>

<p>You can set tags on a bug as you are reporting it. For example, if
you are including a patch with your bug report, you may wish to set
the <code>patch</code> tag.  This is not required, and the developers
will set tags on your report as and when it is appropriate.

<p>To set tags, put a line like this one in the
<a href="#pseudoheader">pseudo-header</a>:</p>

<pre>
Tags: &lt;<var>tags</var>&gt;
</pre>

<p>Replace &lt;<var>tags</var>&gt; with one or more of the available tags,
as described in the
<a href="Developer.html#tags">developers' documentation</a>.
Separate multiple tags with commas, spaces, or both.

<pre>
User: &lt;<var>username</var>&gt;
Usertags: &lt;<var>usertags</var>&gt;
</pre>

<p>Replace &lt;<var>usertags</var>&gt; with one or more usertags.
Separate multiple tags with commas, spaces, or both. If you specify a
username, that users tags will be set. Otherwise, the email address of
the sender will be used as the username</p>


<h2>Not forwarding to the maintainer</h2>

<!--
<p>If a bug report is minor (for example, a documentation typo or other
trivial build problem), or you're submitting many reports at once,
send them to <code>maintonly</code> at <code>debbugs.gnu.org</code> or
<code>quiet</code> at <code>debbugs.gnu.org</code>.
<code>maintonly</code> will send the report on to the package
maintainer (provided you supply a correct <code>Package</code> line in
the pseudo-header and the maintainer is known), and
-->

<p>If you send your initial report to <code>quiet</code>
at <code>debbugs.gnu.org</code>, the system will not forward it
anywhere at all but only file it as a bug (useful if, for example, you
are submitting many similar bugs and want to post only a summary).

<p>If you do this the bug system will set the <code>Reply-To</code> of
any forwarded message so that replies will by default be processed in
the same way as the original report.


<h2>Acknowledgements</h2>

<p>Normally, the bug system will return an acknowledgement to you by
e-mail when you report a new bug or submit additional information to an
existing bug. If you want to suppress this acknowledgement, include an
<code>X-Debbugs-No-Ack</code> header in your e-mail (the contents of this
header do not matter; it can be in the mail header or
in the pseudo-header with the <code>Package</code> field). If
you report a new bug with this header, you will need to check the web
interface yourself to find the bug number.</p>

<p>Note that this header will not suppress acknowledgements from the
<code>control</code> at <code>debbugs.gnu.org</code> mailserver, since those acknowledgements
may contain error messages which should be read and acted upon.</p>


<h3>bug reports against unknown packages</h3>

<p>If the bug tracking system doesn't know who the maintainer of the
relevant package is it'll forward the report to
the help-debbugs mailing list. <!-- even if <code>maintonly</code> was used.-->

<!--
<p>When sending to <code>maintonly</code> at <code>debbugs.gnu.org</code> or
<var>nnn</var><code>-maintonly@debbugs.gnu.org</code> you should make sure that
the bug report is assigned to the right package, by putting a correct
<code>Package</code> at the top of an original submission of a report,
or by using <a href="server-control.html">the
<code>control</code> at <code>debbugs.gnu.org</code> service</a> to (re)assign the report
appropriately first if it isn't correct already.
-->


<hr>

<p>Other pages:
<ul>
  <li><a href="./">Bug tracking system main contents page.</a>
  <li><a href="Developer.html">Developers' information regarding the bug processing system.</a>
  <li><a href="Access.html">Accessing the bug tracking logs other than by WWW.</a>
  <li><a href="db/ix/full.html">Full list of outstanding and recent bug reports.</a>
  <li><a href="db/ix/packages.html">Packages with bug reports.</a>
  <li><a href="db/ix/maintainers.html">Maintainers of packages with bug reports.</a>

</ul>

<HR>
 <ADDRESS>
 Last modified:
 <!--timestamp-->
 Thu, 31 Dec 2009 15:56:38 UTC
 <!--timestamp-->
	  
 <P>
 <A HREF="//debbugs.gnu.org/">Debian bug tracking system</A><BR>
 Copyright (C) 1999 Darren O. Benham,
 1997 nCipher Corporation Ltd,
 1994-97 Ian Jackson.
 </ADDRESS>


