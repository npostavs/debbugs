               Accessing bug reports in the tracking system logs

   Each message received at or sent by the bug processing system is logged
   and made available in a number of ways.

   There is a mailserver which can send bug reports as plain text on request.
   To use it send the word help as the sole contents of an email to request
   (the Subject of the message is ignored), or read the instructions on the
   World Wide Web or in the file bug-log-mailserver.txt.

     ----------------------------------------------------------------------

   Other pages:
     * bug tracking system main contents page.
     * Instructions for reporting bugs.
     * Developers' information regarding the bug processing system.
     * Full list of outstanding and recent bug reports.
     * Packages with bug reports.
     * Maintainers of packages with bug reports.

     ----------------------------------------------------------------------

    Emacs maintainers <bug-gnu-emacs@gnu.org>. Last modified: Thu, 31 Dec
    2009 15:56:38 UTC

    Debian bug tracking system
    Copyright (C) 1999 Darren O. Benham, 1997 nCipher Corporation Ltd,
    1994-97 Ian Jackson.
